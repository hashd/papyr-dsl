#### papyr-dsl
Upcoming DSL for Papyr

[![Build Status](https://travis-ci.org/hashd/papyr-dsl.svg?branch=master)](https://travis-ci.org/hashd/papyr-dsl)
[![Code Climate](https://codeclimate.com/github/codeclimate/codeclimate/badges/gpa.svg)](https://codeclimate.com/github/codeclimate/codeclimate)
[![Test Coverage](https://codeclimate.com/github/hashd/papyr-dsl/badges/coverage.svg)](https://codeclimate.com/github/papyr/papyr-dsl/coverage)

Papyr-dsl will power the soul of Papyr once its finished. Its still in very early stages of development.

##### Setup
You'll need `node 6 or above` to work on this project.

> - Install Node 6 or above for your respective platform
> - Install the following `npm` packages globally
>      `concurrently watch-run mocha rollup`
> - Install the npm dependencies by running the following command
>      `npm install`
> - You should be good to go, just run the following command
>      `npm run start`