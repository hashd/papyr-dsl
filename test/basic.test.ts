import * as mocha from 'mocha'
import * as chai from 'chai'
import { Greeter } from '../src'

const should = chai.should

should()

describe('Setup test', () => {
  it('should initialize correctly', () => {
    'string'.should.have.property('should')
  })

  it('should greet correctly', () => {
    const greeter = new Greeter('Papyr')
    greeter.greet().should.equal('Hello, Papyr')
  })
})
