import * as mocha from 'mocha'
import * as chai from 'chai'
import { Node, node, line, htmlAdapter } from '../../../src/svg'

chai.should()

describe('Transform to HTML', () => {
  it('should transform svg node correctly', () => {
    let svgNode = node('svg')
    htmlAdapter.transform(svgNode).should.equal('<svg></svg>\n')
  })

  it('should transform a nested node correctly', () => {
    let svgNode = node('svg')
    svgNode.addChild(node('g').addChild(node('a')))
    svgNode.addChild(node('a'))
    svgNode.addChild(line(0, 0, 4, 4))
    htmlAdapter.transform(svgNode).should.equal(
`<svg>
\t<g>
\t\t<a></a>
\t</g>
\t<a></a>
\t<line x1="0" y1="0" x2="4" y2="4"></line>
</svg>
`)
  })
})