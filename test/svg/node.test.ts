import * as mocha from 'mocha'
import * as chai from 'chai'
import { Node, node, line } from '../../src'
import { List } from 'immutable'

chai.should()

describe('Node creation', () => {
  it('should create svg node using Constructor', () => {
    let svgNode = new Node('svg')
    svgNode.type.should.equal('svg')
  })

  it('should create svg node using factory', () => {
    let svgNode = node('svg')
    svgNode.type.should.equal('svg')
  })
})

describe('Node manipulation', () => {
  it('should add nodes correctly', () => {
    let svgNode = node('svg')
    svgNode.addChild(node('g'))
    svgNode.addChildren(List<Node>([node('a'), undefined, line(0, 0, 4, 4)]))

    svgNode.children.should.have.property('size').equals(3)
    svgNode.children.map(node => node ? node.type: undefined).toArray().should.eql(['g', 'a', 'line'])
    svgNode.should.equal(svgNode.children.get(0).parent)
  })
})