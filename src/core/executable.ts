import { StringMap } from '../types'

export interface Executable<T> {
  execute(scope: object, args?: StringMap): T | null
}