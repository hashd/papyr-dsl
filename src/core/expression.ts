import { Executable, Operator } from './'

export class Expression<T> implements Executable<T> {
  operator: Operator<T>
  operands: { [key: string]: any }

  execute(scope: Object): T | null {
    return this.operator.apply(this.operands)
  }
}