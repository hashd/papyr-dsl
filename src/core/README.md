### Papyr DSL Core
Core language constructs of the DSL will reside in here.

- **Executable**: defines an interface for a unit of execution like an expression or a function
- **Expression**: defines an implementation of the simplest form of an executable, an expression