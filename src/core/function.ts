import { Operator, Executable, Expression } from './'
import { StringMap } from '../types'

export class Function<T> implements Operator<T>, Executable<T> {
  steps: Executable<any>[]

  constructor() {
    this.steps = []
  }

  add(step: Executable<any>): Function<T> {
    this.steps.push(step)
    return this
  }

  remove(idx: number): Function<T> {
    this.steps.splice(idx, 1)
    return this
  }

  apply(args: StringMap): T | null {
    return this.steps.map(step => step.execute(args)).slice(-1)[0]
  }

  execute(scope: object, args: StringMap): T | null {
    return null
  }
}