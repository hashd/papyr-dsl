import { StringMap } from '../types'

export interface Operator<T> {
  apply(args: StringMap): T | null
}