import { Node, DecoratorA, DecoratorB, ClassDecoratorA } from './svg'

@ClassDecoratorA('Papyr')
export class Greeter {
  name: string
  base: Node

  constructor(name: string) {
    this.name = name
    this.base = new Node(name)
  }

  @DecoratorB()
  @DecoratorA()
  greet() {
    return `Hello, ${this.name}`
  }
}

export * from './core'
export * from './svg'