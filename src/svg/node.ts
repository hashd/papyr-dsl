import { StringMap } from '../types'
import { List, Map } from 'immutable'

export interface NodeInterface {
  type: string
  props: Map<String, String>
  children: List<Node>
}

export interface NodeAdapter<T> {
  transform(node: Node): T
}

export class Node implements NodeInterface {
  type: string
  props: Map<String, String>
  children: List<Node>
  parent: Node | null

  constructor(type: string, props = Map<String, String>(), children = List<Node>()) {
    this.type = type
    this.props = props
    this.children = children
    this.parent = null
  }

  addChild(child: Node): Node {
    this.children = this.children.push(child.setParent(this))
    return this
  }

  addChildren(nodes = List<Node>()): Node {
    this.children = this.children.concat(nodes.map(node => node ? node.setParent(this): undefined).filter(node => node !== undefined)).toList()
    return this
  }

  addProp(key: string, value: string): Node {
    this.props = this.props.set(key, value)
    return this
  }

  addProps(props = Map<String, String>()): Node {
    this.props = this.props.concat(props).toMap()
    return this
  }

  setParent(node: Node): Node {
    this.parent = node
    return this
  }
}

export function node(type: string, props = Map<String, String>(), children = List<Node>()): Node {
  return new Node(type, props, children)
}