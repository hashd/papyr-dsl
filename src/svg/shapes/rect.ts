import { Node, node } from '../node'
import { Map } from 'immutable'

export function rect(x: number, y: number, height: number, width: number): Node {
  return node('rect', Map<String, String>({ x, y, width, height }))
}