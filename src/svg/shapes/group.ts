import { Node, node } from '../node'

export function group(): Node {
  return node('g')
}