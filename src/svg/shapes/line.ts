import { Node, node } from '../node'
import { Map } from 'immutable'

export function line(x1: number, y1: number, x2: number, y2: number): Node {
  return node('line', Map<String, String>({ x1, y1, x2, y2 }))
}