function DecoratorA() {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
  }
}

function DecoratorB() {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
  }
}

function ClassDecoratorA(prop: string) {
  return function ClassDecoratorA<T extends {new(...args:any[]):{}}>(constructor: T) {
    return class extends constructor {
      name: string = prop
    }
  }
}

export { DecoratorA, DecoratorB, ClassDecoratorA }