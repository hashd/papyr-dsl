import { Map } from 'immutable'
import { Node, NodeAdapter } from '../node'

class HTMLAdapter implements NodeAdapter<string> {
  transform(node: Node): string {
    return this.toHTML(node)
  }

  private toHTML(node: Node, level: number = 0): string {
    const indent = Array(level + 1).join('\t'),
          props = node.props.reduce((acc, v, k) => acc + ` ${k}="${v}"`, ''),
          childrenHTMLArray = node.children.map(node => node ? this.toHTML(node, level + 1): ''),
          childrenHTML = childrenHTMLArray.join('')

    return `${indent}<${node.type}${props}>${node.children.size?'\n':''}${childrenHTML}${node.children.size?indent:''}</${node.type}>\n`
  }
}

export const htmlAdapter = new HTMLAdapter()