import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'
import typescript from 'rollup-plugin-typescript'

export default {
  entry: 'src/index.ts',
  format: 'umd',
  moduleName: 'papyr-dsl',
  plugins: [
    commonjs({
      include: 'node_modules/**',
      namedExports: {
        'node_modules/immutable/dist/immutable.js': [ 'Map', 'Set', 'List' ]
      }
    }),
    resolve(),
    typescript({
      typescript: require('typescript')
    })
  ],
  dest: 'dist/papyr-dsl.js'
}